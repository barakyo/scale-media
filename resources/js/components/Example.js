import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { DataGrid } from "@material-ui/data-grid";
import Button from "@material-ui/core/Button";
import axios from "axios";
import { makeStyles, TextField } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

const ProductListing = () => {
  const [productPages, setProductPages] = useState({});
  const [page, setPage] = useState(1);

  async function fetchProducts() {
    const productResponse = await axios.get(`/api/products?page=${page}`);
    console.log("fetchProducts:productResponse", productResponse);
    const { data } = productResponse;
    const newProductPages = {
      ...productPages,
      [page]: data,
    };
    setProductPages(newProductPages);
  }

  useEffect(() => {
    fetchProducts(page);
  }, [page]);

  const onPageChange = (params) => {
    const { page: pageParam } = params;
    if (pageParam < page) {
      setPage(page - 1);
    } else {
      setPage(page + 1);
    }
  };

  const productPage = productPages[page];

  if (!productPage) {
    return null;
  }

  const { total, data: products } = productPage;

  const columns = [
    { field: "id", headerName: "ID" },
    { field: "name", headerName: "Name", width: 150 },
    { field: "price", headerName: "Price", width: 150 },
    { field: "description", headerName: "Description", width: 300 },
  ];

  return (
    <div style={{ height: 700, width: "100%" }}>
      <DataGrid
        columns={columns}
        rows={products}
        rowCount={total}
        page={page - 1}
        pageSize={25}
        pagination
        onPageChange={onPageChange}
        rowsPerPageOptions={[25]}
        paginationMode="server"
      />
    </div>
  );
};

const useAddEditFormStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
}));

const AddEditForm = (props) => {
  const { setIsEdting } = props;
  const classes = useAddEditFormStyles();
  const [errors, setErrors] = useState({});
  const [productName, setProductName] = useState("");
  const [productPrice, setProductPrice] = useState("");
  const [productDescription, setProductDescription] = useState("");

  const onChange = (event) => {
    console.log("onmChange", event.target.id, event.target.value);
    const { id, value } = event.target;
    switch (id) {
      case "name": {
        setProductName(value);
        break;
      }
      case "price": {
        setProductPrice(value);
        break;
      }
      case "description": {
        setProductDescription(value);
        break;
      }
    }
  };

  const onSubmit = () => {
    const formErrors = {};
    if (productName == "") {
      formErrors.name = true;
    }

    if (productPrice == "") {
      formErrors.price = true;
    }

    if (productDescription == "") {
      formErrors.description = true;
    }

    if (Object.keys(formErrors).length > 0) {
      setErrors(formErrors);
    } else {
      const data = {
        name: productName,
        price: productPrice,
        description: productDescription,
      };
      axios.post("/api/products", data);
      setIsEdting(false);
    }
  };

  return (
    <form className={classes.root}>
      <Typography variant="h6" className={classes.title}>
        Add/Edit Product
      </Typography>
      <TextField
        required
        error={errors.name}
        id="name"
        label="Product Name (Required)"
        onChange={onChange}
        value={productName}
      />
      <br />
      <TextField
        required
        id="price"
        error={errors.price}
        label="Product Price (Required)"
        onChange={onChange}
        value={productPrice}
      />
      <br />
      <TextField
        id="description"
        error={errors.description}
        label="Multiline"
        multiline
        value={productDescription}
        onChange={onChange}
        rows={4}
      />
      <br />
      <Button color="primary" variant="contained" onClick={onSubmit}>
        Add Product
      </Button>
    </form>
  );
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
}));

const App = (props) => {
  const [isEditing, setIsEdting] = useState(false);
  const classes = useStyles();

  const startEditing = () => {
    setIsEdting(true);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            Scale Media
          </Typography>

          <Button variant="contained" color="secondary" onClick={startEditing}>
            + Product
          </Button>
        </Toolbar>
      </AppBar>
      {!isEditing && <ProductListing />}
      {isEditing && <AddEditForm setIsEdting={setIsEdting} />}
    </div>
  );
};

export default App;

if (document.getElementById("example")) {
  ReactDOM.render(<App />, document.getElementById("example"));
}
