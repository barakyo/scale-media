#! /bin/env fish

for i in (seq 80 100)
    echo $i
    curl -X POST http://localhost:80/api/products -d "{\"name\": \"product-$i\", \"description\": \"product-$i\", \"price\": 13.37}" -H 'Accept: application/json' -H 'Content-Type: application/json'
    sleep 1
end