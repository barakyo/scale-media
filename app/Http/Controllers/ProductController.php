<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Http\Requests\ProductPostRequest;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return Product::paginate(25)->withQueryString();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ProductPostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductPostRequest $request)
    {

        $validated = $request->validated();

        $product = Product::create([
            'name' => $validated['name'],
            'description' => $validated['description'],
            'price' => $validated['price'],
        ]);

        $product->save();

        return $product->toArray();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id - id of the product to fetch
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return Product::find($id)->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id - id of the product to update
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $product = Product::find($id);

        if (!$product) {
            return response()->json(
                [
                    'status' => 'error',
                    'code' => 'NOT_FOUND',
                    'message' => "Could not find product with id {$id}",
                ],
                400
            );
        }

        $product->update($request->json()->all());

        return $product->toArray();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id - id of the product to delete
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $product = Product::find($id);

        if (!$product) {
            return response()->json(
                [
                    'status' => 'error',
                    'code' => 'NOT_FOUND',
                    'message' => "Could not find product with id {$id}",
                ],
                400
            );
        }

        $product->delete();
        return $product->toArray();
    }
}
