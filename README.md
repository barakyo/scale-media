# Scale Media

## Background

This is my submission for the Scale Media technical assessment. For this submission, I chose to use Laravel as the framework for the backend and React as the view layer on the frontend. Since there was a 2 hour time limit, I didn't focus on writing the cleanest code and tried to push through as many features as possible. In addition, I tried not to get too hung up on any single feature to try to avoid wasting too much time on one thing. In retrospect, I was pretty good about this on the backend but not so much on the frontend. That being said, I feel like I was able to accomplish a good amount of the requirements.

### Plan of Attack

I will not lie, I took a small liberty to review the challenge and Laravel documentation prior to starting my 2-hour timer. It had been some time since I've used Laravel and PHP so I had to get back up to speed with the ecosystem. While I'm very familiar with MVC like frameworks in other languages (Django, Play!, Phoenix), I wanted to map those concepts to Laravel concepts so I wasn't spending time going through the docs in the 2-hour time limit.

I created a plan of attack to try to accomplish as much as possible, which was:

- Generate Project
- Generate Product model
- Fill in `$fillable` on model
- Generate Post Request
- Add Paginate endpoint
- Add Post request endpoint
- Add Update endpoint
- Add delete endpoint
- Start on frontend

## Requirements

This assignment was broken down into 2 sections an API portion and a UI portion.

### API

The API portion requiredd that a Product API be built with the following endpoints:

- `GET /products`: Paginated index endpoint to retrieve all products
- `POST /products`: Endpoint to create a product
- `PUT /products/:id`: Endpoint to update a product
- `DELETE /products/:id`: Endpoint to Delete a product

### UI

The requirements for the UI portion was to have 3 views:

- Listing View
- Product View
- Edit product View

## Requirements

To start the application you will need PHP, artisan, docker and docker-compose. As part of Laravel's setup, I chose to use [Sail](https://laravel.com/docs/8.x/sail). Sail provides a CLI for managing all of Laravel's dependencies to easily get a project up and running.

Once you've cloned the application, you'll need to install sail using:

```shell
php artisan sail:install
```

### Web Server

Once sail is installed, you'll need to have two terminal windows open to start the processes for the front and backend.

To start the dev server, you'll want to run the command:

```shell
./vendor/bin/sail up
```

### Frontend

You'll first need to install the frontend dependencies using the following command:o

```shell
./vendor/bin/sail npm install
```

To build the frontend assets (and watch), you'll want to run the command:

```shell
./vendor/bin/sail npm run watch
```

### Migrations

Once you've started the two docker containers (Webserver & PostgreSQL), you'll need to run migrations, to do that, you'll want to run the command:

```shell
./vendor/bin/sail artisan migrate
```

### Seeding

Apologies, but I didn't have time to write a proper seed script and I whipped up something quick in Fish shell (my shell of choice) which is the `seed.fish` file.

If by any chance you do happen to use fish shell, you can seed the app with products by running:o

```
./seed.fish
```

## Database Schema

As described in the previous section, this application uses PostgreSQL. It defines a single table in the public schema with the following DDL:

```
scale_media=# \d products
                                          Table "public.products"
   Column    |              Type              | Collation | Nullable |               Default
-------------+--------------------------------+-----------+----------+--------------------------------------
 id          | bigint                         |           | not null | nextval('products_id_seq'::regclass)
 name        | character varying(255)         |           | not null |
 description | character varying(255)         |           | not null |
 price       | numeric(8,2)                   |           | not null |
 created_at  | timestamp(0) without time zone |           |          |
 updated_at  | timestamp(0) without time zone |           |          |
Indexes:
    "products_pkey" PRIMARY KEY, btree (id)
```

## API

The API exposed by the backend is very similar to the one described with the requirements. The API endpoints are prefixed with `/api/`.

### Get Products

The GET Products endpoint exposes a paginated endpoint which returns 25 products at a time.

### Query Params

**HEADS UP!** While the requirements called to implement a `limit` query parameter, I did not get a chance to implement it.

| Key  | Type   | Description                                     |
| ---- | ------ | ----------------------------------------------- |
| page | int    | The page to fetch                               |
| sort | string | The value to sort by (name, price, description) |

### Example Requests

#### Fetch first 25 products

```shell
curl http://localhost:80/api/products
```

#### Fetch a specific page

```shell
curl http://localhost:80/api/products?page=2
```

#### Sort by a parameter

```shell
curl http://localhost:80/api/products?sort=name
```

### Get Product

The GET Product endpoint is an endpoint to fetch a single product using the product's ID.

### Example Request

```shell
curl "http://localhost:80/api/products/1"
```

### Create Product

The Create Product endpoint exposes an endpoint that accepts a POST request and JSON body to create a product.

#### Request Body

| Key         | Type    | Required | Description                    |
| ----------- | ------- | -------- | ------------------------------ |
| name        | string  | Yes      | The name of the product        |
| price       | decimal | Yes      | The price of the product       |
| description | string  | Yes      | The description of the product |

#### Example Request

**HEADS UP!** Laravel is _very_ picky about headers, so if youd on't include the `Accept` Header it won't process the request.

```shell
curl -X POST \
    http://localhost:80/api/products \
    -d '{"name": "foo2", "description": "foo", "price": 13.37}' \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json'
```

### Update Product

The API exposes a PUT endpoint on `/api/products/:id` which allows you to update all or partial fields of a product. This API accepts the product's ID in the URL parameters and will update the specified product if it exists.

### Example request

```shell
curl -X PUT http://localhost:80/api/products/1 -d '{"name": "product-1"}' -H 'Accept: application/json'
```

### Delete Product

Finally, the API exposes an endpoint to delete a product using a `DELETE` request. This endpoint accepts the product's ID in the URL parameters and will delete the specified product if it exists.

### Example Request

```shell
curl -X DELETE http://localhost:80/api/products/2 -H 'Accept: application/json' -H 'Content-Type: application/json'
```

## Frontend

For the frontend, I decided to use React and Material-UI. I used the based component generated by Laravel and just started hacking away. I didn't want to mess with React-Router or other configurations, so the application is very bare-bones.

The React application can be found: `resources/js/components/Example.js`. The application bootstraps the `App` component which starts with the listing page by default. This is toggled by the `isEditing` false.

I use Material UI's DataGrid component to try to quickly render a product listing. Users are able to scroll through products using the arrows which make API requests to the server and render the products to the page.

When a user hits the `+ Product` button at the top right, it toggles the `isEditing` flag and displays a form which allows you to add a product. The form has very little validation and doesn't provide any sort of success or error response after submitted.

## Retrospective

I believe having the Plan of Attack was a good idea as I was quickly able to complete the backend piece. In addition, I also stuck to my advice of not spending too much time on any one feature for the backend, but unfortunately, I didn't stick to that advice when it came to the frontend.

As you can tell, my frontend plan of attack wasn't very well thought out. I simply just jumped in and tried to get as much done as possible. While I was able to get the initial page of products rendered quickly, the pagination and playing with the component props took me sometime to find the right configuration. While I'm familiar w/ Material-UI, this component was one that I was less familiar with. I personally believe I spent a little too much on the pagination which kept me from building out the Add and Edit form. By the time I decided to switch to the Add/Edit form, I had about ~20 minutes left on my timer. I quickly switched gears and tried to whip something together as quick as possible for the form.
